#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <GL/freeglut.h>
#include <GL/gl.h>
#include <iostream>
#include "AppController.h"
#include "Rectangle.h"
#include "Button.h"

class Controller : public AppController {
    Button* addBtn;

    // Rectangle r;
    Rectangle* r;

public:
    Controller(){
        // Initialize your state variables 
        addBtn = new Button("Add", -0.9, -0.7);
        r = nullptr;
    }

    void leftMouseDown( float x, float y ){
        if (y < -0.7){
            if (addBtn->contains(x, y)){
                std::cout << "Clicked add" << std::endl;
                if (r == nullptr){
                    r = new Rectangle();
                }
                else{
                    Rectangle* temp = r;
                    while (temp->neighbor != nullptr){
                        temp = temp->neighbor;
                    }

                    // At this point, temp is pointing to the
                    // last rectangle in the collection

                    temp->neighbor = new Rectangle();

                }
                // if (r == nullptr){
                //     r = new Rectangle();
                // }
                // else if (r->neighbor == nullptr){
                //     r->neighbor = new Rectangle();
                // }
                // else if (r->neighbor->neighbor == nullptr){
                //     r->neighbor->neighbor = new Rectangle();
                // }
                
            }
        }
        else{
            Rectangle* temp = r;
            while (temp != nullptr){
                temp->deselect();
                temp = temp->neighbor;
            }

            temp = r;
            while (temp != nullptr){
                if (temp->contains(x, y)){
                    temp->select();
                    break;
                }
                temp = temp->neighbor;
            }


            // if (r && r->contains(x, y)){
            //     r->select();
            // }
            // else if (r && r->neighbor && r->neighbor->contains(x, y)){
            //     r->neighbor->select();
            // }
        }
    } 

    void mouseMotion(float x, float y){
        if (y > -0.7){
            Rectangle* temp = r;
            while(temp != nullptr){
                if (temp->isSelected()){
                    temp->setX(x);
                    temp->setY(y);
                }
                temp = temp->neighbor;
            }
            // if (r && r->isSelected()){
            //     r->setX(x);
            //     r->setY(y);
            // }
            // else if (r && r->neighbor && r->neighbor->isSelected()){
            //     r->neighbor->setX(x);
            //     r->neighbor->setY(y);
            // }
        }
    }

    void render() {
        // Render some graphics

        addBtn->draw();


        Rectangle* temp = r;

        while (temp != nullptr){
            temp->draw();
            temp = temp->neighbor;
        }

        // if (r){ // if (r != nullptr)
        //     r->draw();
        // }
        // if (r && r->neighbor){ // if (r != nullptr)
        //     glBegin(GL_LINES);
        //     glVertex2f(r->getX() + r->getW()/2, r->getY() - r->getH()/2);
        //     glVertex2f(r->neighbor->getX() + r->neighbor->getW()/2, r->neighbor->getY() - r->neighbor->getH()/2);
        //     glEnd();
        //     r->neighbor->draw();
        // }
        // if (r && r->neighbor && r->neighbor->neighbor){
        //     glBegin(GL_LINES);
        //     glVertex2f(r->neighbor->getX() + r->neighbor->getW()/2, r->neighbor->getY() - r->neighbor->getH()/2);
        //     glVertex2f(r->neighbor->neighbor->getX() + r->neighbor->neighbor->getW()/2, r->neighbor->neighbor->getY() - r->neighbor->neighbor->getH()/2);
        //     glEnd();
        //     r->neighbor->neighbor->draw();
        // }
    }

};

#endif